﻿using Microsoft.WindowsAzure.MobileServices;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{

        DataObjects.User User = new DataObjects.User() { Type = 0 };
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

		public Login ()
		{
			InitializeComponent();
            AuthRegSwitch.Toggled += AuthRegSwitch_Toggled;
            LoginEntry.Completed += LoginEntry_TextChenged;
            TypeSwitch.Toggled += TypeSwitch_Togle;
            LoginButton.Clicked += Authorization;
            RegButton.Clicked += Registartion;
            Global.Token = CrossSettings.Current.GetValueOrDefault<string>("token", "none");
            Global.UserType = CrossSettings.Current.GetValueOrDefault<string>("type", "none");
            CheckAuth();
        }

        async void Authorization(object sender, EventArgs e) // авторизация
        {
            Active.IsVisible = true;
            Active.IsRunning = true;
            var arguments = new Dictionary<string, string>
            {
                {"Action","auth" },
                {"Username",LoginEntry.Text },
                {"Password",PasswordEntry.Text },
                {"FirstName", "none" },
                {"LastName", "none" },
                {"Type","none" },
                {"Token",Global.Token }
            };
            string result = "none";
            try
            {
                //LoadInd.IsRunning = true;
                result = await client.InvokeApiAsync<string>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch(Exception ex)
            {
                //LoadInd.IsRunning = false;
                await Global.Error(this);
            }
            if (result=="none")
            {
                //LoadInd.IsRunning = false;
                await DisplayAlert("Авторизация", "Неверное имя пользователя и/или пароль", "Пробовать снова");
            }
            else
            {
                Global.Token = result;
                CrossSettings.Current.AddOrUpdateValue<string>("token", result);
                arguments = new Dictionary<string, string>
                {
                    {"Action","type" },
                    {"Username","none" },
                    {"Password","none" },
                    {"FirstName", "none" },
                    {"LastName", "none" },
                    {"Type","none" },
                    {"Token",Global.Token }
                };
                try
                {
                    //LoadInd.IsRunning = true;
                    result = await client.InvokeApiAsync<string>("Auth", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    //LoadInd.IsRunning = false;
                    await Global.Error(this);
                }
                //LoadInd.IsRunning = false;
                Global.UserType = result;
                CrossSettings.Current.AddOrUpdateValue<string>("type", result);
                var Menu = new Menu();
                NavigationPage.SetHasNavigationBar(Menu, false);
                await Navigation.PushAsync(Menu);
            }
            Active.IsVisible = false;
            Active.IsRunning = false;
        }

        async void CheckAuth() // проверка авторизации
        {
            Active.IsVisible = true;
            Active.IsRunning = true;
            var arguments = new Dictionary<string, string>
            {
                {"Action","chktkn"},
                {"Token",Global.Token },
                {"Username","none" },
                {"FirstName", "none" },
                { "LastName","none"},
                {"Password", "none" }
            };
            bool result = false;
            try
            {
                //LoadInd.IsRunning = true;
                result = await client.InvokeApiAsync<bool>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                //LoadInd.IsRunning = false;
                await DisplayAlert("Соединение с сервером", "Произошла ошибка соединения с сервером. Проверьте интернет-соединение", "ОК");
            }
            if (result == true)
            {
                //LoadInd.IsRunning = false;
                var Menu = new Menu();
                NavigationPage.SetHasNavigationBar(Menu, false);
                await Navigation.PushAsync(Menu);
            }
            Active.IsVisible = false;
            Active.IsRunning = false;
        }

        async void Registartion(object sender, EventArgs e) // регистрация
        {
            Active.IsVisible = true;
            Active.IsRunning = true;
            var arguments = new Dictionary<string, string>
            {
                {"Action","reg"},
                {"Username",LoginEntry.Text },
                {"Password",PasswordEntry.Text },
                {"FirstName",FirstNameEntry.Text },
                {"LastName",LastNameEntry.Text },
                {"Type",User.Type.ToString() },
                {"Token","none" }
            };
            string result = "none";
            try
            {
                //LoadInd.IsRunning = true;
                result = await client.InvokeApiAsync<string>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch(Exception ex)
            {
                //LoadInd.IsRunning = false;
                await Global.Error(this);
            }
            if (result == "none")
            {
                //LoadInd.IsRunning = false;
                CorrectNameLabel.IsVisible = true;
            }
            else
            {
                //LoadInd.IsRunning = false;
                Global.Token = result;
                Global.UserType = User.Type.ToString();
                CrossSettings.Current.AddOrUpdateValue<string>("token", result);
                CrossSettings.Current.AddOrUpdateValue<string>("type", Global.UserType);
                await Navigation.PushAsync(new Menu());
            }
            Active.IsVisible = false;
            Active.IsRunning = false;
        }

        async void LoginEntry_TextChenged(object sender, EventArgs e)
        {
            User.Username = LoginEntry.Text;
            var argument = new Dictionary<string, string>
            {
                {"Action","chknm"},
                {"Token",Global.Token},
                {"Username", User.Username},
                {"FirstName", "none" },
                {"LastName","none"},
                {"Password", "none" }
            };
            if (AuthRegSwitch.IsToggled)
            {
                bool CorrectName=false;
                try
                {
                    CorrectName = await client.InvokeApiAsync<bool>("Auth", System.Net.Http.HttpMethod.Post, argument);
                }
                catch(Exception ex)
                {
                    await Global.Error(this);
                }
                if (CorrectName == false)
                {
                    CorrectNameLabel.IsVisible = true;
                }
                else
                {
                    CorrectNameLabel.IsVisible = false;
                }
            }
        }

        void AuthRegSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            if (e.Value == true)
            {
                LoginButton.IsVisible = false;
                RegButton.IsVisible = true;
                FirstNameEntry.IsVisible = true;
                LastNameEntry.IsVisible = true;
                TypeStack.IsVisible = true;
            }
            else
            {
                LoginButton.IsVisible = true;
                RegButton.IsVisible = false;
                FirstNameEntry.IsVisible = false;
                LastNameEntry.IsVisible = false;
                TypeStack.IsVisible = false;
            }
        }

        void TypeSwitch_Togle(object sender, ToggledEventArgs e)
        {
            if (TypeSwitch.IsToggled)
            {
                User.Type = 1;
            }
            else
            {
                User.Type = 0;
            }
        }
	}
}
