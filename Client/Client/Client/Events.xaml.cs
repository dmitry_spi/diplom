﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Events : ContentPage
	{
        private List<string[]> Classes = new List<string[]>();
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

        public Events ()
		{
			InitializeComponent ();
            NewEventButton.Clicked += (s, e) =>
            {
                NewEventStack.IsVisible = true;
                NewEventButton.IsVisible = false;
            };
            if (Global.UserType == "1")
            {
                NewEventButton.IsVisible = true;
            }
            AddEventButton.Clicked += (s, e) =>
            {
                AddEvent();
            };
            NameEventEntry.TextChanged += (s, e) =>
              {
                  if (NameEventEntry.Text == "" || PlaceEventEntry.Text == "")
                  {
                      AddEventButton.IsEnabled = false;
                  }
                  else
                  {
                      AddEventButton.IsEnabled = true;
                  }
              };
            PlaceEventEntry.TextChanged += (s, e) =>
            {
                if (NameEventEntry.Text == "" || PlaceEventEntry.Text == "")
                {
                    AddEventButton.IsEnabled = false;
                }
                else
                {
                    AddEventButton.IsEnabled = true;
                }
            };
            GenClasses();
            GetEvents();
        }

        public async void GetEvents()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Token", Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("Event", System.Net.Http.HttpMethod.Post, arguments);
                EventsStack.Children.Clear();
                foreach(var item in result)
                {
                    if (Global.UserType == "1")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                          {
                              DelEvent(item[0]);
                          };
                        var label = new Label() { Text = item[1], FontSize=Device.GetNamedSize(NamedSize.Small,typeof(Label)) };
                        label.GestureRecognizers.Add(tap);
                        EventsStack.Children.Add(label);
                    }
                    else
                    {
                        EventsStack.Children.Add(new Label() { Text=item[1], FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)) });
                    }
                    EventsStack.Children.Add(new Label() { Text = item[2] });
                    EventsStack.Children.Add(new Label() { Text = item[3] });
                    EventsStack.Children.Add(new Label() { Text = item[4] });
                }
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
        }

        public async void AddEvent()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","add" },
                {"Token", Global.Token },
                {"Name", NameEventEntry.Text },
                {"When", DateEventEntry.Date.ToString("dd MMM yyyy", new CultureInfo("ru-UA"))+" в "+TimeEventEntry.Time.Hours.ToString()+" часов "+TimeEventEntry.Time.Minutes.ToString()+" минут"},
                {"Where", PlaceEventEntry.Text },
                {"ClassKey", Classes.Count()>0?Classes[ClassPicker.SelectedIndex][0]:"0"},
                {"EventId", "none" }
            };
            try
            {
                ActiveNow.IsVisible = true;
                await client.InvokeApiAsync<bool>("Event", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
                NewEventStack.IsVisible = false;
                NewEventButton.IsVisible = true;
            }
            GetEvents();
        }

        public async void DelEvent(string EventId)
        {
            var answer = await DisplayAlert("Удаление", "Удалить событие?", "Да", "Нет");
            if (answer == true)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","del" },
                    {"Token", Global.Token },
                    {"Name", "none" },
                    {"When", "none" },
                    {"Where", "none" },
                    {"ClassKey", "none"},
                    {"EventId", EventId }
                };
                try
                {
                    ActiveNow.IsVisible = true;
                    await client.InvokeApiAsync<bool>("Event", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                finally
                {
                    ActiveNow.IsVisible = false;
                }
                GetEvents();
            }
        }

        private async void GenClasses() // загрузка классов
        {

            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    Classes.Add(myclasses);
                    ClassPicker.Items.Add(myclasses[1]);
                }
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            if (Classes.Count > 0)
            {
                ClassPicker.IsEnabled = true;
                ClassPicker.SelectedIndex = 0;
            }
        }
    }
}
