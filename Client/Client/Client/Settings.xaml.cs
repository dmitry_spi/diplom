﻿using Microsoft.WindowsAzure.MobileServices;
using Plugin.Settings;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Settings : ContentPage
	{
        DataObjects.User User = new DataObjects.User() { Type = 0 };
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

        public Settings ()
		{
			InitializeComponent ();
            ExitButton.Clicked += ExitButton_Clicked;
            SaveButton.Clicked += SaveButton_Clicked;
            ChengePasswordButton.Clicked += ChengePasswordButton_Clicked;
            AdsNotifSwitch.Toggled += (s, e) =>
            {
                if (AdsNotifSwitch.IsToggled)
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckAds", true);
                else
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckAds", false);
            };
            EventsNotifSwitch.Toggled += (s, e) =>
            {
                if (EventsNotifSwitch.IsToggled)
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckEvents", true);
                else
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckEvents", false);
            };
            MessagesNotifSwitch.Toggled += (s, e) =>
            {
                if (MessagesNotifSwitch.IsToggled)
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckMessages", true);
                else
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckMessages", false);
            };
            RecordsNotifSwitch.Toggled += (s, e) =>
            {
                if (RecordsNotifSwitch.IsToggled)
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckRecords", true);
                else
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckRecords", false);
            };
            SchedulesNotifSwitch.Toggled += (s, e) =>
            {
                if (SchedulesNotifSwitch.IsToggled)
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckSchedules", true);
                else
                    CrossSettings.Current.AddOrUpdateValue<bool>("CheckSchedules", false);
            };
            EnableNotifSwitch.Toggled += (s, e) =>
            {
                if (EnableNotifSwitch.IsToggled)
                {
                    //if (Device.OS == TargetPlatform.Android)
                    //{
                    //    var intent = new Android.Content.Intent(new Droid.MainActivity(), typeof(Droid.BackgroundServ));
                    //    var service = new Droid.BackgroundServ();
                    //    service.StartService(intent);
                    //}
                    //if (Device.OS == TargetPlatform.Windows)
                    //{
                    //    ;
                    //}
                }
            };
            GetNotifSettings();
            GetUserData();
        }

        private void GetNotifSettings()
        {
            if (CrossSettings.Current.GetValueOrDefault<bool>("CheckAds",false))
            {
                AdsNotifSwitch.IsToggled = true;
            }
            if (CrossSettings.Current.GetValueOrDefault<bool>("CheckEvents", false))
            {
                EventsNotifSwitch.IsToggled = true;
            }
            if (CrossSettings.Current.GetValueOrDefault<bool>("CheckMessages", false))
            {
                MessagesNotifSwitch.IsToggled = true;
            }
            if (CrossSettings.Current.GetValueOrDefault<bool>("CheckRecords", false))
            {
                RecordsNotifSwitch.IsToggled = true;
            }
            if (CrossSettings.Current.GetValueOrDefault<bool>("CheckSchedules", false))
            {
                SchedulesNotifSwitch.IsToggled = true;
            }
        }

        private async void GetUserData()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","firstname" },
                {"Username","none" },
                {"Password","none" },
                {"FirstName", "none" },
                {"LastName", "none" },
                {"Type","none" },
                {"Token",Global.Token }
            };
            string result="none";
            try
            {
                result = await client.InvokeApiAsync<string>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            FirstNameEntry.Text = result;
            Global.UserFirstName = result;
            arguments = new Dictionary<string, string>
            {
                {"Action","lastname" },
                {"Username","none" },
                {"Password","none" },
                {"FirstName", "none" },
                {"LastName", "none" },
                {"Type","none" },
                {"Token",Global.Token }
            };
            try
            {
                result = await client.InvokeApiAsync<string>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            LastNameEntry.Text = result;
            Global.UserLastName = result;
        }

        private async void ChengePasswordButton_Clicked(object sender, EventArgs e)
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","chgpwd" },
                {"Token",Global.Token },
                {"FirstName","none" },
                {"LastName","none" },
                {"Username","none" },
                {"Password", NewPasswordEntry.Text }
            };
            if (NewPasswordEntry.Text == NewRepeatPasswordEntry.Text)
            {
                bool result = false;
                try
                {
                    result = await client.InvokeApiAsync<bool>("Auth", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                if (result == true)
                {
                    await DisplayAlert("Изменение пароля", "Пароль успешно изменён", "ОК");
                }
                else
                {
                    await DisplayAlert("Изменение пароля", "Не удалось изменить пароль", "ОК");
                }
            }
            else
            {
                await DisplayAlert("Изменение пароля", "Пароли не совпадают", "ОК");
            }
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","chgdt" },
                {"Token",Global.Token },
                {"FirstName",FirstNameEntry.Text },
                {"LastName",LastNameEntry.Text },
                {"Username","none" },
                {"Password", "none" }
            };
            bool result = false;
            try
            {
                result = await client.InvokeApiAsync<bool>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            if (result==true)
            {
                await DisplayAlert("Изменение данных", "Данные успешно сохранены", "ОК");
            }
            else
            {
                await DisplayAlert("Изменение данных", "Не удалось сохранить данные", "ОК");
            }
        }

        public async void ExitButton_Clicked(object sender, EventArgs e)
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","goout"},
                {"Token",Global.Token },
                {"Username","none" },
                {"FirstName", "none" },
                { "LastName","none"},
                {"Password", "none" }
            };
            bool result=false;
            try
            {
                result = await client.InvokeApiAsync<bool>("Auth", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            if (result == true)
            {
                Global.Token = "none";
                CrossSettings.Current.AddOrUpdateValue<string>("token", "none");
                await Navigation.PopToRootAsync();
            }
        }
	}
}
