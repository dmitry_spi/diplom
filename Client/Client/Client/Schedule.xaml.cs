﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Schedule : ContentPage
	{
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);
        private List<string[]> Classes = new List<string[]>();
        public Schedule ()
		{
			InitializeComponent ();
            for (int i = 1; i<11; i++)
            {
                MonNumPicker.Items.Add(i.ToString());
                TueNumPicker.Items.Add(i.ToString());
                WedNumPicker.Items.Add(i.ToString());
                ThiNumPicker.Items.Add(i.ToString());
                FriNumPicker.Items.Add(i.ToString());
                SatNumPicker.Items.Add(i.ToString());
            }
            var tapMondey = new TapGestureRecognizer();
            tapMondey.Tapped += (s, e) =>
            {
                Mon.IsVisible = true;
            };
            Monday.GestureRecognizers.Add(tapMondey);
            var tapTuesday = new TapGestureRecognizer();
            tapTuesday.Tapped += (s, e) =>
            {
                Tue.IsVisible = true;
            };
            Tuesday.GestureRecognizers.Add(tapTuesday);
            var tapWednesday = new TapGestureRecognizer();
            tapWednesday.Tapped += (s, e) =>
            {
                Wed.IsVisible = true;
            };
            Wednesday.GestureRecognizers.Add(tapWednesday);
            var tapThirsday = new TapGestureRecognizer();
            tapThirsday.Tapped += (s, e) =>
            {
                Thi.IsVisible = true;
            };
            Thirsday.GestureRecognizers.Add(tapThirsday);
            var tapFriday = new TapGestureRecognizer();
            tapFriday.Tapped += (s, e) =>
            {
                Fri.IsVisible = true;
            };
            Friday.GestureRecognizers.Add(tapFriday);
            var tapSaturday = new TapGestureRecognizer();
            tapSaturday.Tapped += (s, e) =>
            {
                Sat.IsVisible = true;
            };
            Saturday.GestureRecognizers.Add(tapSaturday);

            MonSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Mon", (MonNumPicker.SelectedIndex+1).ToString(), MonSubjEntry.Text, Global.UserType=="1"?Classes[MonClassPicker.SelectedIndex][0]:"-");
                Mon.IsVisible = false;
            };
            TueSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Tue", (TueNumPicker.SelectedIndex + 1).ToString(), TueSubjEntry.Text, Global.UserType == "1" ? Classes[TueClassPicker.SelectedIndex][0] : "-");
                Tue.IsVisible = false;
            };
            WedSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Wed", (WedNumPicker.SelectedIndex + 1).ToString(), WedSubjEntry.Text, Global.UserType == "1" ? Classes[WedClassPicker.SelectedIndex][0] : "-");
                Wed.IsVisible = false;
            };
            ThiSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Thi", (ThiNumPicker.SelectedIndex + 1).ToString(), ThiSubjEntry.Text, Global.UserType == "1" ? Classes[ThiClassPicker.SelectedIndex][0] : "-");
                Thi.IsVisible = false;
            };
            FriSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Fri", (FriNumPicker.SelectedIndex + 1).ToString(), FriSubjEntry.Text, Global.UserType == "1" ? Classes[FriClassPicker.SelectedIndex][0] : "-");
                Fri.IsVisible = false;
            };
            SatSaveBotton.Clicked += (s, e) =>
            {
                AddSubj("Sat", (SatNumPicker.SelectedIndex + 1).ToString(), SatSubjEntry.Text, Global.UserType == "1" ? Classes[SatClassPicker.SelectedIndex][0] : "-");
                Sat.IsVisible = false;
            };
            if (Global.UserType == "1")
            {
                GenClasses();
            }
            GetSubj();
        }

        private async void GetSubj() // загрузка расписания
        {
            // словарь параметров
            var arguments = new Dictionary<string, string>
            {
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                //запрос
                var result = await client.InvokeApiAsync<string[][]>("Schedules", System.Net.Http.HttpMethod.Post, arguments);
                // очищаем
                MonStack.Children.Clear();
                TueStack.Children.Clear();
                WedStack.Children.Clear();
                ThiStack.Children.Clear();
                FriStack.Children.Clear();
                SatStack.Children.Clear();
                // выводим на каждый день недели
                foreach (var item in result)
                {
                    if (item[0] == "Mon")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) => // создём действие на событие "тап"
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2]+" / Класс: "+item[3]}; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)); // устанавливаем шрифт
                        label.GestureRecognizers.Add(tap); //  привязываем действие на событие
                        MonStack.Children.Add(label); // добавляем лейбл на экран
                    }
                    if (item[0] == "Tue")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] + " / Класс: " + item[3] }; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                        label.GestureRecognizers.Add(tap);
                        TueStack.Children.Add(label);
                    }
                    if (item[0] == "Wed")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] + " / Класс: " + item[3] }; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                        label.GestureRecognizers.Add(tap);
                        WedStack.Children.Add(label);
                    }
                    if (item[0] == "Thi")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] + " / Класс: " + item[3] }; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                        label.GestureRecognizers.Add(tap);
                        ThiStack.Children.Add(label);
                    }
                    if (item[0] == "Fri")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] + " / Класс: " + item[3] }; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                        label.GestureRecognizers.Add(tap);
                        FriStack.Children.Add(label);
                    }
                    if (item[0] == "Sat")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Subject_Tapped(item[0], item[1], item[3], item[4]);
                        };
                        Label label = new Label();
                        if (Global.UserType == "1")
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] + " / Класс: " + item[3] }; // создаём лейбл
                        }
                        else
                        {
                            label = new Label() { Text = item[1] + ". " + item[2] }; // создаём лейбл
                        }
                        label.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                        label.GestureRecognizers.Add(tap);
                        SatStack.Children.Add(label);
                    }
                }
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
        }

        private async void DelSubj(string DayOfWeek, string Number)
        {
            var answer = await DisplayAlert("Удаление", "Удалить элемент?", "Да", "Нет");
            if (answer == true)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","del" },
                    {"Token", Global.Token },
                    {"DayOfWeek", DayOfWeek },
                    {"Number",Number },
                    {"Subj","none" },
                    {"ClassKey","none" }
                };
                try
                {
                    ActiveNow.IsVisible = true;
                    var result = await client.InvokeApiAsync<bool>("Schedules", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch(Exception ex)
                {
                    await Global.Error(this);
                }
                finally
                {
                    ActiveNow.IsVisible = false;
                }
                GetSubj();
            }
        }

        private async void AddSubj(string DayOfWeek, string Number, string Subj, string ClassKey)
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","add" },
                {"Token", Global.Token },
                {"DayOfWeek", DayOfWeek },
                {"Number",Number },
                {"Subj",Subj },
                {"ClassKey", ClassKey}
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<bool>("Schedules", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            GetSubj();
        }

        private async void Subject_Tapped(string Dayofweek, string Number, string ClassName, string LastName)
        {
            if (LastName == Global.UserLastName)
            {
                var inform_result = await DisplayAlert("Информация", "Класс: " + ClassName + "/" + LastName, "OK", "Удалить");
                if (inform_result == false)
                {
                    DelSubj(Dayofweek, Number);
                }
            }
            else
            {
                await DisplayAlert("Информация", "Класс: " + ClassName + "/" + LastName, "OK");
            }
        }

        private async void GenClasses() // загрузка классов
        {

            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            MonClassPicker.Items.Clear();
            TueClassPicker.Items.Clear();
            WedClassPicker.Items.Clear();
            ThiClassPicker.Items.Clear();
            FriClassPicker.Items.Clear();
            SatClassPicker.Items.Clear();
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    Classes.Add(myclasses);
                    MonClassPicker.Items.Add(myclasses[1]);
                    TueClassPicker.Items.Add(myclasses[1]);
                    WedClassPicker.Items.Add(myclasses[1]);
                    ThiClassPicker.Items.Add(myclasses[1]);
                    FriClassPicker.Items.Add(myclasses[1]);
                    SatClassPicker.Items.Add(myclasses[1]);
                }
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            if (Classes.Count() > 0)
            {
                MonClassPicker.IsVisible = true;
                TueClassPicker.IsVisible = true;
                WedClassPicker.IsVisible = true;
                ThiClassPicker.IsVisible = true;
                FriClassPicker.IsVisible = true;
                SatClassPicker.IsVisible = true;
                MonClassPicker.SelectedItem = 0;
                TueClassPicker.SelectedItem = 0;
                WedClassPicker.SelectedItem = 0;
                ThiClassPicker.SelectedItem = 0;
                FriClassPicker.SelectedItem = 0;
                SatClassPicker.SelectedItem = 0;
            }
        }
    }
}
