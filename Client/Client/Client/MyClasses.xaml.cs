﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyClasses : ContentPage
	{
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

        public MyClasses ()
		{
			InitializeComponent ();
            NewClassButton.Clicked += NewClassButton_Clicked;
            NewClassSaveButton.Clicked += NewClasssSaveButon_clicked;
            GenClasses();
		}

        private async void NewClasssSaveButon_clicked(object sender, EventArgs e)
        {
            if (Global.UserType=="0") // для ученика
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","enterclass" },
                    {"Token",Global.Token },
                    {"ClassName","none" },
                    {"ClassKey",NewClassCodeEntry.Text }
                };
                bool result = false;
                try
                {
                    result = await client.InvokeApiAsync<bool>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch(Exception ex)
                {
                    await Global.Error(this);
                }
                if (result==true)
                {
                    await DisplayAlert("Новый класс", "Класс успешно добавлен", "OK");
                    GenClasses();
                }
                else
                {
                    await DisplayAlert("Новый класс", "Не удалось добавить класс. Возможно, неверный код класса или такой класс уже добавлен", "OK");
                }
            }
            if (Global.UserType=="1") // для учителя
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","newclass" },
                    {"Token",Global.Token },
                    {"ClassName",NewClassNameEntry.Text },
                    {"ClassKey","none" }
                };
                bool result = false;
                try
                {
                    result = await client.InvokeApiAsync<bool>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                if (result == true)
                {
                    await DisplayAlert("Новый класс", "Новый класс успешно добавлен", "OK");
                    GenClasses();
                }
                else
                {
                    await DisplayAlert("Новый класс", "Не удалось добавить новый класс", "OK");
                }
            }
        }

        private async void NewClassButton_Clicked(object sender, EventArgs e)
        {
            if (Global.UserType=="0")
            {
                NewClassCodeEntry.IsVisible = true;
            }
            if (Global.UserType == "1")
            {
                NewClassNameEntry.IsVisible = true;
                await DisplayAlert("Совет", "Желательно, чтобы имя класса содержало информацию об учителе и предмете. Таким образом ученики смогут легко ориентироваться в классах.", "OK");
            }
            NewClassSaveButton.IsVisible = true;
            NewClassButton.IsVisible = false;
        }

        private async void GenClasses() // загрузка классов
        {
            List<Button> Classes = new List<Button>();
            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            try
            {
                while (ClassesStack.Children.Count()>0)
                {
                    ClassesStack.Children.Remove(ClassesStack.Children.First());
                }
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    var button = new Button() { Text = myclasses[1], FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button))};
                    button.Clicked += (object sender, EventArgs e) =>
                    {
                        ShowCode(myclasses[0]);
                    };
                    ClassesStack.Children.Add(button);
                }
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
        }

        private async void ShowCode(string Key)
        {
            var result = await DisplayAlert("Уникальный код класса", Key, "OK", "Покинуть класс");
            if (result == false)
            {
                result = await DisplayAlert("Удалить", "Вы действительно хотите покинуть класс?", "Да", "Нет");
                if (result == true)
                {
                    DelClass(Key);
                }
            }
        }

        private async void DelClass(string Key) //удалить класс
        {
            bool result = false;
            var arguments = new Dictionary<string, string>
            {
                {"Action","exitclass" },
                {"Token",Global.Token },
                {"ClassName",""},
                {"ClassKey",Key }
            };
            try
            {
                result = await client.InvokeApiAsync<bool>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            if (result == true)
            {
                await DisplayAlert("Удалить", "Класс успешно удалён", "ОК");
                GenClasses();
            }
            else
            {
                await DisplayAlert("Удалить", "Не удалось удалить класс", "ОК");
            }
        }
	}
}