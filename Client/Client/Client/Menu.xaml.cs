﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Menu : ContentPage
	{
        MainTabbedPage mtpage = new MainTabbedPage();
        
        public Menu ()
		{
			InitializeComponent ();
            MyClasses.Clicked += Open_MyClasses;
            Schedule.Clicked += Open_Schedule;
            Ads.Clicked += Open_Ads;
            Events.Clicked += Open_Events;
            Messages.Clicked += Open_Messages;
            MyNotes.Clicked += Open_Records;
            Settings.Clicked += Open_Settings;
            NavigationPage.SetHasNavigationBar(mtpage, false);
        }
        private void Open_MyClasses(object sender, EventArgs e)
        {
            mtpage.GoToPage("myclasses");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Schedule(object sender, EventArgs e)
        {
            mtpage.GoToPage("schedule");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Ads(object sender, EventArgs e)
        {
            mtpage.GoToPage("ads");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Messages(object sender, EventArgs e)
        {
            mtpage.GoToPage("messages");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Events(object sender, EventArgs e)
        {
            mtpage.GoToPage("events");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Records(object sender, EventArgs e)
        {
            mtpage.GoToPage("records");
            Navigation.PushAsync(mtpage);
        }
        private void Open_Settings(object sender, EventArgs e)
        {
            mtpage.GoToPage("settings");
            Navigation.PushAsync(mtpage);
        }
    }
}
