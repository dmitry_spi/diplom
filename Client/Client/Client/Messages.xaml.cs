﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Messages : ContentPage
	{
        private List<string[]> Classes = new List<string[]>();
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);
        int classListId = 0;
        string LastText="none";
        object Lock = new object();
        bool runupdate = false;

        public Messages ()
		{
			InitializeComponent ();
            ClassPicker.SelectedIndexChanged += ClassPicker_Chenged;
            SendMessageButton.Clicked += (object sender, EventArgs e) =>
              {
                  SendMessage();
              };
            MessageEntry.TextChanged += (object sender, TextChangedEventArgs e) =>
              {
                 if (MessageEntry.Text!="")
                  {
                      SendMessageButton.IsEnabled = true;
                  }
                 else
                  {
                      SendMessageButton.IsEnabled = false;
                  }
              };
            GenClasses();
        }

        private async void ClassPicker_Chenged(object sender, EventArgs e)
        {
            classListId = ClassPicker.SelectedIndex;
            MessagesStack.Children.Clear();
            LastText = "";
            await GenMessages();
            if (runupdate == false)
            {
                runupdate = true;
                MessagesUpdate();
            }
        }

        private async void MessagesUpdate()
        {
            while (true)
            {
                if (runupdate)
                {
                    await GenMessages();
                }
                //await MessageScroller.ScrollToAsync(0, MessagesStack.Height, false);
            }
        }

        private async void GenClasses() // загрузка классов
        {
            
            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    Classes.Add(myclasses);
                    ClassPicker.Items.Add(myclasses[1]);
                }
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            if (Classes.Count > 0)
            {
                ClassPicker.IsEnabled = true;
                ClassPicker.SelectedIndex = 0;
            }
        }

        private async Task GenMessages() // загрузка сообщений
        {
            if (Classes.Count() > 0)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Token",Global.Token },
                    {"ClassKey", Classes[classListId][0]},
                    {"Text",LastText}
                };
                try
                {
                    ActiveNow.IsVisible = true;
                    var result = await client.InvokeApiAsync<string[][]>("Messaging", System.Net.Http.HttpMethod.Post, arguments);
                    if (result.Count() > 0)
                    {
                        MessagesStack.Children.Clear();
                    }
                    foreach (string[] message in result)
                    {
                        LastText = message[0];
                        MessagesStack.Children.Insert(0, new Label() { Text = message[0], TextColor=Color.Black });
                        MessagesStack.Children.Insert(0, new Label() { Text = message[1] + " " + message[2] + ":", FontAttributes = FontAttributes.Bold, TextColor=Color.Black });
                    }
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                finally
                {
                    ActiveNow.IsVisible = false;
                }
            }
            await Task.Delay(10000);
        }

        private async void SendMessage()
        {
            SendMessageButton.IsEnabled = false;
            var arguments = new Dictionary<string, string>
            {
                {"Action","newmes" },
                {"Token", Global.Token },
                {"Text",MessageEntry.Text },
                {"ClassKey",Classes[classListId][0] }
            };
            MessageEntry.Text = "";
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<bool>("Messaging", System.Net.Http.HttpMethod.Post, arguments);
                MessagesStack.Children.Insert(0, new Label() { Text = MessageEntry.Text, TextColor = Color.Black });
                MessagesStack.Children.Insert(0, new Label() { Text = Global.UserFirstName + " " + Global.UserLastName + ":", FontAttributes = FontAttributes.Bold, TextColor = Color.Black });
                MessageEntry.Text = "";
            }
            catch(Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            //await MessageScroller.ScrollToAsync(0, MessagesStack.Height, true);
        }
    }
}
