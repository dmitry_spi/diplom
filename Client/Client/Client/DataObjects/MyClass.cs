﻿namespace Client.DataObjects
{
    public class MyClass
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Key { get; set; }
    }
}