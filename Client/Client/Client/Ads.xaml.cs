﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Ads : ContentPage
	{
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);
        private List<string[]> Classes = new List<string[]>();

        public Ads ()
		{
			InitializeComponent ();

            if (Global.UserType=="1")
            {
                GenClasses();
                AddButton.IsVisible = true;
                AddButton.Clicked += (s, e) =>
                {
                    ClassPicker.IsVisible = true;
                    AdEditor.IsVisible = true;
                    SaveAdButton.IsVisible = true;
                    AddButton.IsVisible = false;
                };
                SaveAdButton.Clicked += AddSaveButton_Clicked;
                AdEditor.TextChanged += (s, e) =>
                {
                    if (AdEditor.Text=="")
                    {
                        SaveAdButton.IsEnabled = false;
                    }
                    else
                    {
                        SaveAdButton.IsEnabled = true;
                    }
                };
            }
            GetAds();
		}

        private async void GetAds()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Token",Global.Token }
            };
            try
            {
                /* await client.InvokeApiAsync<string[][]>("Ads", System.Net.Http.HttpMethod.Post, arguments)
                 * [0] - Id
                 * [1] - Text
                 * [2] - Class name
                 * [3] - Author last name
                 * [4] - Date
                 */
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("Ads", System.Net.Http.HttpMethod.Post, arguments);
                AdsStack.Children.Clear();
                foreach (var item in result)
                {
                    AdsStack.Children.Add(new Label { Text = item[2] + " | " + item[3]});
                    AdsStack.Children.Add(new Label { Text = item[4] });
                    var label = new Label() { Text = item[1], FontSize = Device.GetNamedSize(NamedSize.Medium,typeof(Label)), TextColor = Color.Black };
                    if (Global.UserType == "1")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            Ads_Clicked(item[0]);
                        };
                        label.GestureRecognizers.Add(tap);
                    }
                    AdsStack.Children.Add(label);
                    AdsStack.Children.Add(new Label());
                }
            }
            catch(Exception ex)
            {
                await Global.Error(this);
                await DisplayAlert("error", ex.ToString(), "OK");
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
        }

        private async void Ads_Clicked(string AdId)
        {
            var answer_result = await DisplayAlert("Удаление", "Удалить запись?", "Да", "Нет");
            if (answer_result==true)
            {
                DelAd(AdId);
            }
        }

        private async void DelAd(string id)
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","del" },
                {"Token",Global.Token },
                {"Text","none" },
                {"ClassKey","none" },
                {"AdId",id }
            };
            try
            {
                ActiveNow.IsVisible = true;
                await client.InvokeApiAsync<bool>("Ads", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            GetAds();
        }

        private async void AddSaveButton_Clicked(object sender, EventArgs e)
        {
            if (Classes.Count() > 0)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","add" },
                    {"Token",Global.Token },
                    {"Text",AdEditor.Text },
                    {"ClassKey",Classes[ClassPicker.SelectedIndex][0] },
                    {"AdId","none" }
                };
                try
                {
                    ActiveNow.IsVisible = true;
                    await client.InvokeApiAsync<bool>("Ads", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                finally
                {
                    ActiveNow.IsVisible = false;
                    ClassPicker.IsVisible = false;
                    AdEditor.Text = "";
                    AdEditor.IsVisible = false;
                    SaveAdButton.IsVisible = false;
                    AddButton.IsVisible = true;
                }
                GetAds();
            }
        }

        private async void GenClasses() // загрузка классов
        {

            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    Classes.Add(myclasses);
                    ClassPicker.Items.Add(myclasses[1]);
                }
                ClassPicker.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            if (Classes.Count > 0)
            {
                ClassPicker.IsEnabled = true;
                ClassPicker.SelectedIndex = 0;
            }
        }
    }
}
