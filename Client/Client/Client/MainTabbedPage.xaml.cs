﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Client
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabbedPage : TabbedPage
    {
        Settings SettingsPage = new Settings();
        Page MyClassesPage = new Page();
        Page SchedulePage = new Page();
        Page AdsPage = new Page();
        Page MessagePage = new Page();
        Page EventsPage = new Page();
        Page RecordsPage = new Page();

        public MainTabbedPage ()
        {
            InitializeComponent();
        }

        public void GoToPage(string param)
        {
            switch (param)
            {
                case "myclasses":
                    Children.Remove(MyClassesPage);
                    MyClassesPage = new MyClasses();
                    Children.Insert(0,MyClassesPage);
                    CurrentPage = MyClassesPage;
                    break;
                case "schedule":
                    Children.Remove(SchedulePage);
                    SchedulePage = new Schedule();
                    Children.Insert(0, SchedulePage);
                    CurrentPage = SchedulePage;
                    break;
                case "ads":
                    Children.Remove(AdsPage);
                    AdsPage = new Ads();
                    Children.Insert(0, AdsPage);
                    CurrentPage = AdsPage;
                    break;
                case "messages":
                    Children.Remove(MessagePage);
                    MessagePage = new Messages();
                    Children.Insert(0, MessagePage);
                    CurrentPage = MessagePage;
                    break;
                case "events":
                    Children.Remove(EventsPage);
                    EventsPage = new Events();
                    Children.Insert(0, EventsPage);
                    CurrentPage = EventsPage;
                    break;
                case "records":
                    Children.Remove(RecordsPage);
                    RecordsPage = new Records();
                    Children.Insert(0, RecordsPage);
                    CurrentPage = RecordsPage;
                    break;
                case "settings":
                    Children.Remove(SettingsPage);
                    Children.Add(SettingsPage);
                    CurrentPage = SettingsPage;
                    break;
            }
        }
    }
}
