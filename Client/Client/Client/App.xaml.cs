﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Client
{
    public partial class App : Application
	{

        public App ()
		{
            var pageStyle = new Style(typeof(ContentPage))
            {
                Setters =
                {
                    new Setter {Property = ContentPage.BackgroundColorProperty, Value = Color.Black},
                }
            };
            var tabbedPageStyle = new Style(typeof(TabbedPage))
            {
                Setters =
                {
                    new Setter {Property = TabbedPage.BackgroundColorProperty, Value = Color.Black},
                }
            };
            var buttonStyle = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromRgb(33,110,255)},
                    new Setter {Property = Button.TextColorProperty, Value = Color.White}
                }
            };
            Resources = new ResourceDictionary();
            Resources.Add("pageStyle", pageStyle);
            Resources.Add("tabbedPageStyle", tabbedPageStyle);
            Resources.Add("buttonStyle", buttonStyle);
            InitializeComponent();
			MainPage = new NavigationPage(new Client.Login());
            NavigationPage.SetHasNavigationBar(MainPage, false);
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
