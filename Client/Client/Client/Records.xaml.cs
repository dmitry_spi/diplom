﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Records : ContentPage
	{
        private List<string[]> Classes = new List<string[]>();
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

        public Records ()
		{
			InitializeComponent ();
            AddRecButton.Clicked += (s, e) =>
            {
                AddRec();
            };
            RecEditor.TextChanged += (s, e) =>
            {
                if (RecEditor.Text=="")
                {
                    AddRecButton.IsEnabled = false;
                }
                else
                {
                    AddRecButton.IsEnabled = true;
                }
            };
            if (Global.UserType == "1")
            {
                GenClasses();
                ClassPicker.IsVisible = true;
            }
            GetRecords();
        }

        public async void GetRecords()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("Records", System.Net.Http.HttpMethod.Post, arguments);
                RecordsStack.Children.Clear();
                foreach (var item in result)
                {
                    RecordsStack.Children.Add(new Label() { Text = item[2] + " | " + item[3] });
                    RecordsStack.Children.Add(new Label() { Text = item[4] });
                    if ((Global.UserType=="0" && item[2]== "Личная запись") || Global.UserType == "1")
                    {
                        var tap = new TapGestureRecognizer();
                        tap.Tapped += (s, e) =>
                        {
                            DelRec(item[0]);
                        };
                        var label = new Label() { Text = item[1], FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))};
                        label.GestureRecognizers.Add(tap);
                        RecordsStack.Children.Add(label);
                    }
                    else
                    {
                        RecordsStack.Children.Add(new Label() { Text = item[1], FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))});
                    }
                    RecordsStack.Children.Add(new Label());
                }
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
        }

        public async void AddRec()
        {
            var arguments = new Dictionary<string, string>
            {
                {"Action","add" },
                {"Token",Global.Token },
                {"Text",RecEditor.Text },
                {"ClassKey", Classes.Count()>0?Classes[ClassPicker.SelectedIndex][0]:"-"},
                {"RecId","none" }
            };
            RecEditor.Text = "";
            try
            {
                ActiveNow.IsVisible = true;
                await client.InvokeApiAsync<bool>("Records", System.Net.Http.HttpMethod.Post, arguments);
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            GetRecords();
        }

        public async void DelRec(string RecId)
        {
            var answer = await DisplayAlert("Удаление", "Удалить элемент?", "Да", "Нет");
            if (answer == true)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Action","del" },
                    {"Token",Global.Token },
                    {"Text","none" },
                    {"ClassKey", "none"},
                    {"RecId",RecId }
                };
                try
                {
                    ActiveNow.IsVisible = true;
                    await client.InvokeApiAsync<bool>("Records", System.Net.Http.HttpMethod.Post, arguments);
                }
                catch (Exception ex)
                {
                    await Global.Error(this);
                }
                finally
                {
                    ActiveNow.IsVisible = false;
                }
                GetRecords();
            }
        }

        private async void GenClasses() // загрузка классов
        {

            var arguments = new Dictionary<string, string>
            {
                {"Action","getclasses" },
                {"Token",Global.Token }
            };
            try
            {
                ActiveNow.IsVisible = true;
                var result = await client.InvokeApiAsync<string[][]>("WorkWithClass", System.Net.Http.HttpMethod.Post, arguments);
                foreach (string[] myclasses in result)
                {
                    Classes.Add(myclasses);
                    ClassPicker.Items.Add(myclasses[1]);
                }
            }
            catch (Exception ex)
            {
                await Global.Error(this);
            }
            finally
            {
                ActiveNow.IsVisible = false;
            }
            if (Classes.Count > 0)
            {
                ClassPicker.IsEnabled = true;
                ClassPicker.SelectedIndex = 0;
            }
        }
    }
}
