﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client
{
    static class Global
    {
        public static string Token { get; set; }
        public static string AppUrl = "http://diplapp.azurewebsites.net";
        public static string UserType { get; set; }
        public static string UserFirstName { get; set; }
        public static string UserLastName { get; set; }

        public async static Task Error(Page page)
        {
            await page.DisplayAlert("Ошибка", "Ой-ой... Произошла ошибка.", "ОК");
        }
    }
}
