﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Plugin.Settings;
using Plugin.LocalNotifications;
using System.Threading.Tasks;

namespace Client.Droid
{
    [Service]
    class BackgroundServ : Service
    {
        MobileServiceClient client = new MobileServiceClient(Global.AppUrl);

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }

        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            CheckUpdate();
            return StartCommandResult.Sticky;
        }

        private async Task CheckUpdate()
        {
            while (true)
            {
                var arguments = new Dictionary<string, string>
                {
                    {"Token",Global.Token },
                    {"Date", CrossSettings.Current.GetValueOrDefault<string>("dateUpdate","01/01/2017")}
                };
                try
                {
                    var result = await client.InvokeApiAsync<List<string>>("Update", System.Net.Http.HttpMethod.Post, arguments);
                    foreach (var item in result)
                    {
                        switch (item)
                        {
                            case "Ads":
                                if (CrossSettings.Current.GetValueOrDefault<bool>("CheckAds", false) == true)
                                {
                                    CrossLocalNotifications.Current.Show("Новое объявление", "Добавлено новое объявление");
                                }
                                break;
                            case "Events":
                                if (CrossSettings.Current.GetValueOrDefault<bool>("CheckEvents", false) == true)
                                {
                                    CrossLocalNotifications.Current.Show("Новое событие", "Добавлено новое событие");
                                }
                                break;
                            case "Messages":
                                if (CrossSettings.Current.GetValueOrDefault<bool>("CheckMessages", false) == true)
                                {
                                    CrossLocalNotifications.Current.Show("Новое сообщение", "Получено новое сообщение");
                                }
                                break;
                            case "Records":
                                if (CrossSettings.Current.GetValueOrDefault<bool>("CheckRecords", false) == true)
                                {
                                    CrossLocalNotifications.Current.Show("Новая запись", "Добавлена новая запись");
                                }
                                break;
                            case "Schedules":
                                if (CrossSettings.Current.GetValueOrDefault<bool>("CheckSchedules", false) == true)
                                {
                                    CrossLocalNotifications.Current.Show("Изменено расписание", "Внесены изменения в расписание");
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ;
                }
                CrossSettings.Current.AddOrUpdateValue<string>("dateUpdate", DateTime.Now.ToString("dd/MM/yyyy"));
                await Task.Delay(60000);
            }
        }
    }
}