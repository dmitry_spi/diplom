﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class SchedulesController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();
        WorkWithClassController wcc = new WorkWithClassController();
        public bool Post(string Action, string Token, string DayOfWeek, string Number, string Subj, string ClassKey)
        {
            switch (Action)
            {
                case "add":
                    return AddSubj(Token, DayOfWeek, Number, Subj, ClassKey);
                case "del":
                    return DelSubj(Token, DayOfWeek, Number);
            }
            return false;
        }

        public string[][] Post(string Token)
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                if (ac.GetType(Token) == 0) // для ученика
                {
                    var keylist = wcc.GetClassKeyList(ac.GetId(Token)); // получаем ключи класса для пользователя
                    // выбираем то расписание, которое создал пользователь
                    var temp_resul = cont.Schedules.Where(sub => sub.UserId == usid);
                    foreach (var key in keylist)
                    {
                        // выбираем расписание для каждого класса пользователя
                        temp_resul = temp_resul.Concat(cont.Schedules.Where(sub => sub.ClassKey == key).Where(sub => sub.UserType == 1));
                    }
                    var result = temp_resul.OrderBy(sub => sub.Number).ToArray();
                    var list = new string[result.Count()][]; // создаём сисок
                    int i = 0;
                    foreach (var item in result) // заполняем список
                    {
                        list[i] = new string[5];
                        list[i][0] = item.DayOfWeek; // день недели
                        list[i][1] = item.Number; // номер
                        list[i][2] = item.Subject; // название предмета
                        var classname = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey); // название класса
                        list[i][3] = classname.Count() > 0 ? classname.First().Name : "-";
                        list[i][4] = ac.GetLastNameById(item.UserId); // фамилия пользователя
                        i++;
                    }
                    return list;
                }
                else
                {
                    int i = 0;
                    var result = cont.Schedules.Where(sub => sub.UserId == usid).OrderBy(sub => sub.Number).ToArray();
                    var list = new string[result.Count()][];
                    foreach (var item in result)
                    {
                        list[i] = new string[5];
                        list[i][0] = item.DayOfWeek;
                        list[i][1] = item.Number;
                        list[i][2] = item.Subject;
                        var classname = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey); // название класса
                        list[i][3] = classname.Count() > 0 ? classname.First().Name : "-";
                        list[i][4] = ac.GetLastNameById(item.UserId); // фамилия пользователя
                        i++;
                    }
                    return list;
                }
            }
            return new string[0][];
        }

        private bool AddSubj(string Token, string DayOfWeek, string Number, string Subj, string ClassKey)
        {
            if (ac.CheckToken(Token))
            {
                var data = new DataObjects.Schedule()
                {
                    Id = Guid.NewGuid().ToString(),
                    DayOfWeek = DayOfWeek,
                    Number = Number,
                    Subject = Subj,
                    UserId = ac.GetId(Token),
                    ClassKey = ClassKey,
                    UserType = ac.GetType(Token)
                };
                cont.Schedules.Add(data);
                cont.SaveChanges();
                // запись обновления
                var updated = new DataObjects.UpdatedList()
                {
                    Id = Guid.NewGuid().ToString(),
                    SchedulesClass = ClassKey
                };
                cont.UpdatedLists.Add(updated);
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private bool DelSubj(string Token, string DayOfWeek, string Number)
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                var data = cont.Schedules.Where(sub => sub.UserId == usid)
                    .Where(sub => sub.DayOfWeek == DayOfWeek)
                    .Where(sub => sub.Number == Number).First();
                cont.Schedules.Remove(data);
                cont.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
