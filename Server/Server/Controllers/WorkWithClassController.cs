﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class WorkWithClassController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();

        public bool Post(string Action, string Token, string ClassName, string ClassKey)
        {
            switch(Action)
            {
                case "newclass":
                    return NewClass(Token, ClassName);
                case "enterclass":
                    return EnterClass(Token, ClassKey);
                case "exitclass":
                    return ExitClass(Token, ClassKey);
            }
            return false;
        }

        public string[][] Post(string Action, string Token)
        {
            switch (Action)
            {
                case "getclasses":
                    return GetMyClasses(Token);
            }
            return new string[0][];
        }

        private string[][] GetMyClasses(string Token) // получить список своих классов
        {
            if (ac.CheckToken(Token) && ac.GetType(Token)==0) // для ученика
            {
                var usid = ac.GetId(Token);
                var listIds = cont.ClassMembers.Where(classmember => classmember.MemberId == usid).ToArray();
                var list = new string[listIds.Count()][];
                int i = 0;
                foreach (DataObjects.ClassMember cm in listIds)
                {
                    var result = cont.MyClasses.Where(myclasses => myclasses.Key == cm.ClassId).First();
                    list[i] = new string[2];
                    list[i][0] = result.Key;
                    list[i][1] = result.Name;
                    i++;
                }
                return list;
            }
            else
            {
                if (ac.CheckToken(Token) && ac.GetType(Token) == 1) // для учителя
                {
                    var usid = ac.GetId(Token);
                    var result = cont.MyClasses.Where(myclasses => myclasses.Author == usid).ToArray();
                    var list = new string[result.Count()][];
                    int i = 0;
                    foreach (DataObjects.MyClass mc in result)
                    {
                        list[i] = new string[2];
                        list[i][0] = mc.Key;
                        list[i][1] = mc.Name;
                        i++;
                    }
                    return list;
                }
            }
            return new string[0][];
        }

        private bool EnterClass(string Token, string ClassKey) // вступление в класс
        {
            if (ac.CheckToken(Token) && ac.GetType(Token)==0)
            {
                var user_id = ac.GetId(Token);
                var isclass = cont.ClassMembers
                    .Where(classme => classme.ClassId == ClassKey)
                    .Where(classmem => classmem.MemberId == user_id);
                if (isclass.Count() > 0)
                {
                    return false;
                }
                var result = cont.MyClasses.Where(myclass => myclass.Key == ClassKey);
                if (result.Count() > 0)
                {
                    var classmember = new DataObjects.ClassMember()
                    {
                        Id = Guid.NewGuid().ToString(),
                        ClassId = ClassKey,
                        MemberId = ac.GetId(Token)
                    };
                    cont.ClassMembers.Add(classmember);
                    cont.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        private bool ExitClass(string Token, string ClassKey) // выход с класса
        {
            if (ac.CheckToken(Token) && ac.GetType(Token)==0) //для ученика
            {
                var usid = ac.GetId(Token);
                var result = cont.ClassMembers.Where(classmember => classmember.ClassId == ClassKey).Where(classmember => classmember.MemberId == usid);
                if (result.Count() > 0)
                {
                    var del_class = result.First();
                    cont.ClassMembers.Remove(del_class);
                    cont.SaveChanges();
                    return true;
                }
            }
            if (ac.CheckToken(Token) && ac.GetType(Token) == 1) // для учителя
            {
                var result_one = cont.MyClasses.Where(myclass => myclass.Key == ClassKey);
                if (result_one.Count() > 0)
                {
                    var del_class = result_one.First();
                    cont.MyClasses.Remove(del_class);
                    cont.SaveChanges();
                    return true;
                }
                var result_two = cont.ClassMembers.Where(classmember => classmember.ClassId == ClassKey);
                if (result_two.Count()>0)
                {
                    foreach (var del_class in result_two)
                    {
                        cont.ClassMembers.Remove(del_class);
                    }
                    cont.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        private bool NewClass(string Token, string ClassName) // создание класса
        {
            if (ac.CheckToken(Token) && ac.GetType(Token)==1)
            {
                var myclass = new DataObjects.MyClass()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = ClassName,
                    Author = ac.GetId(Token),
                    Key = GenClassKey()
                };
                cont.MyClasses.Add(myclass);
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private string GenClassKey()
        {
            Random rand = new Random();
            string key = rand.Next(100000000, 999999999).ToString();
            while (CheckKey(key)==true)
            {
                key = rand.Next(100000000, 999999999).ToString();
            }
            return key;
        }

        public bool CheckKey(string Key) // проверка наличия ключа
        {
            var result = cont.MyClasses.Where(myclass => myclass.Key == Key);
            if (result.Count()>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckAuth(string Key, string UserId)
        {
            var result = cont.MyClasses.Where(myclass => myclass.Key == Key).Where(myclass => myclass.Author == UserId).Count();
            if (result>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetAuthIdByKey(string Key)
        {
            var result = cont.MyClasses.Where(myclass => myclass.Key == Key).First().Author;
            return result;
        }

        public string[] GetClassKeyList(string UserId) // список классов для ученика
        {
            var result = cont.ClassMembers.Where(classmem => classmem.MemberId == UserId);
            if (result.Count()>0)
            {
                var list = new string[result.Count()];
                int i = 0;
                foreach (var key in result)
                {
                    list[i] = key.ClassId;
                    i++;
                }
                return list;
            }
            return new string[0];
        }

        public string[] GetClassKeyListForAuth(string UserId) // список классов для учителя
        {
            var result = cont.MyClasses.Where(myclass => myclass.Author == UserId);
            if (result.Count() > 0)
            {
                var list = new string[result.Count()];
                int i = 0;
                foreach (var key in result)
                {
                    list[i] = key.Key;
                    i++;
                }
                return list;
            }
            return new string[0];
        }

    }
}
