﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class EventController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();
        WorkWithClassController wcc = new WorkWithClassController();

        public bool Post(string Action, string Token, string Name, string When, string Where, string ClassKey, string EventId)
        {
            switch (Action)
            {
                case "add":
                    return AddEvent(Token, Name, When, Where, ClassKey);
                case "del":
                    return DelEvent(Token, EventId);
            }
            return false;
        }

        public string[][] Post(string Token)
        {
            if (ac.CheckToken(Token)) // проверяем токен
            {
                var ClassKeys = ac.GetType(Token) == 0 ? wcc.GetClassKeyList(ac.GetId(Token)) : wcc.GetClassKeyListForAuth(ac.GetId(Token));
                string first_class_key = ClassKeys.Count() > 0 ? ClassKeys[0] : "0";
                var temp_result = cont.Events.Where(evn => evn.ClassKey == first_class_key);
                if (ClassKeys.Count() > 1)
                {
                    foreach (var key in ClassKeys.Skip(1))
                    {
                        temp_result = temp_result.Concat(cont.Events.Where(evn => evn.ClassKey == key));
                    }
                }
                if (temp_result.Count() > 0)
                {
                    var result = temp_result.OrderByDescending(evn => evn.CreatedAt).ToArray(); // сортируем и делаем массив
                    //формируем список
                    var list = new string[result.Count()][];
                    int i = 0;
                    foreach (var item in result)
                    {
                        list[i] = new string[5];
                        list[i][0] = item.Id;
                        list[i][1] = item.Name;
                        list[i][2] = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey).First().Name; // название класса
                        list[i][3] = item.When;
                        list[i][4] = item.Where;
                        i++;
                    }
                    return list;
                }
            }
            return new string[0][];
        }

        private bool AddEvent(string Token, string Name, string When, string Where, string ClassKey) // добавляем запись
        {
            if (ac.CheckToken(Token) && wcc.CheckAuth(ClassKey, ac.GetId(Token))) // проверяем токен и владельца класса
            {
                var data = new DataObjects.Event //создаём запись
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = Name,
                    When = When,
                    Where = Where,
                    ClassKey = ClassKey
                };
                cont.Events.Add(data); // добавляем
                cont.SaveChanges(); // сохраняем
                // запись обновления
                var updated = new DataObjects.UpdatedList()
                {
                    Id = Guid.NewGuid().ToString(),
                    EventsClass = ClassKey
                };
                cont.UpdatedLists.Add(updated);
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private bool DelEvent(string Token, string EventId) // удаляем запись
        {
            if (ac.CheckToken(Token))
            {
                var result = cont.Events.Where(evn => evn.Id == EventId); // находим запись по ID
                if (result.Count() > 0)
                {
                    var data = result.First();
                    if (wcc.CheckAuth(data.ClassKey, ac.GetId(Token))) // проверяем автора
                    {
                        cont.Events.Remove(data); // извлекаем запись
                        cont.SaveChanges(); // сохраняем
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
