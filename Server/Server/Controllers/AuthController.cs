﻿using Microsoft.Azure.Mobile.Server.Config;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
//using Server.DataObjects;
using System;

namespace Server.Controllers
{
    [MobileAppController]
    public class AuthController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();

        public bool Post(string Action, string Token, string Username, string FirstName, string LastName, string Password)
        {
            switch (Action)
            {
                case "chktkn": // проверка активности сессии
                    return CheckToken(Token);
                case "chknm": // проверка валидности имени
                    return CheckName(Username);
                case "goout": // выход
                    return GoOut(Token);
                case "chgdt": // изменить данные пользователя
                    return ChangeData(Token, FirstName, LastName);
                case "chgpwd": // изменить пароль
                    return ChangePassword(Token, Password);
            }
            return false;
        }

        public string Post(string Action, string Username, string Password, string FirstName, string LastName, string Type, string Token)
        {
            switch (Action)
            {
                case "auth": // авторизация
                    return Authorization(Username, Password);
                case "reg": // регистрация
                    return Registration(Username, Password, FirstName, LastName, Type);
                case "firstname":
                    return GetFirstName(Token);
                case "lastname":
                    return GetLastName(Token);
                case "type":
                    return GetType(Token).ToString();
        }
            return "none";
        }

        private bool ChangePassword(string Token, string Password)
        {
            var ChangePasswordUser = cont.Users.Where(user => user.Token == Token);
            if (ChangePasswordUser.Count()>0)
            {
                ChangePasswordUser.First().Password = Password;
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private bool ChangeData(string Token, string FirstName, string LastName)
        {
            var ChangeUser = cont.Users.Where(user => user.Token == Token);
            if (ChangeUser.Count()>0)
            {
                ChangeUser.First().FirstName = FirstName;
                ChangeUser.First().LastName = LastName;
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private string Registration(string Username, string Password, string FirstName, string LastName, string Type) // регистрация
        {
            if (CheckName(Username))
            {
                var Tkn = Guid.NewGuid().ToString();
                var RegUser = new DataObjects.User()
                {
                    Id = Guid.NewGuid().ToString(),
                    Username = Username,
                    Password = Password,
                    FirstName = FirstName,
                    LastName = LastName,
                    Type = int.Parse(Type),
                    Token = Tkn
                };
                cont.Users.Add(RegUser);
                cont.SaveChanges();
                return Tkn;
            }
            else
                return "none";
            
        }

        private string Authorization(string Username, string Password) // авторизация
        {
            var AuthUser = cont.Users.Where(user => user.Username == Username).
                        Where(user => user.Password == Password);
            if (AuthUser.Count() > 0)
            {
                AuthUser.First().Token = Guid.NewGuid().ToString();
                cont.SaveChanges();
                return AuthUser.First().Token;
            }
            else
            {
                return "none";
            }
        }

        public bool CheckToken(string Token) // проверка авторизации
        {
            var CheckUser = cont.Users.Where(user => user.Token == Token).Count();
            if (CheckUser > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetType(string Token)
        {
            if (CheckToken(Token))
            {
                var result = cont.Users.Where(user => user.Token == Token);
                return result.First().Type;
            }
            return -1;
        }

        public string GetId(string Token)
        {
            if (CheckToken(Token))
            {
                var result = cont.Users.Where(user => user.Token == Token);
                return result.First().Id;
            }
            return "none";
        }

        private bool CheckName(string Username) // проверка валидности имени
        {
            var CheckNameUser = cont.Users.Where(user => user.Username == Username).Count();
            if (CheckNameUser > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool GoOut(string Token) // выход пользователя (true - выход выполнен, false - не выполнен)
        {
            var GoOutUser = cont.Users.Where(user => user.Token == Token);
            if (GoOutUser.Count() > 0)
            {
                var GOU = GoOutUser.First();
                GOU.Token = "some";
                cont.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetFirstName(string Token) // получить имя
        {
            var FirstName = cont.Users.Where(user => user.Token == Token);
            if (FirstName.Count()>0)
            {
                return FirstName.First().FirstName;
            }
            return "none";
        }

        public string GetLastName(string Token) // получить фамилию
        {
            var LastName = cont.Users.Where(user => user.Token == Token);
            if (LastName.Count() > 0)
            {
                return LastName.First().LastName;
            }
            return "none";
        }

        public string GetFirstNameById(string UsId) // получить имя
        {
            var FirstName = cont.Users.Where(user => user.Id == UsId);
            if (FirstName.Count() > 0)
            {
                return FirstName.First().FirstName;
            }
            return "none";
        }

        public string GetLastNameById(string UsId) // получить фамилию
        {
            var LastName = cont.Users.Where(user => user.Id == UsId);
            if (LastName.Count() > 0)
            {
                return LastName.First().LastName;
            }
            return "none";
        }
    }
}
