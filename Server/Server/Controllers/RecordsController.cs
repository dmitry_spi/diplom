﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class RecordsController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();
        WorkWithClassController wcc = new WorkWithClassController();
        public bool Post(string Action, string Token, string Text, string ClassKey, string RecId)
        {
            switch (Action)
            {
                case "add":
                    return AddSubj(Token, Text, ClassKey);
                case "del":
                    return DelSubj(Token, RecId);
            }
            return false;
        }

        public string[][] Post(string Token)
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                if (ac.GetType(Token) == 0) // для ученика
                {
                    var keylist = wcc.GetClassKeyList(ac.GetId(Token)); // получаем ключи класса для пользователя
                    // выбираем то расписание, которое создал пользователь
                    var temp_resul = cont.Records.Where(rec => rec.UserId == usid);
                    foreach (var key in keylist)
                    {
                        // выбираем расписание для каждого класса пользователя
                        temp_resul = temp_resul.Concat(cont.Records.Where(rec => rec.ClassKey == key).Where(rec => rec.UserType == 1));
                    }
                    var result = temp_resul.OrderByDescending(rec => rec.CreatedAt).ToArray();
                    var list = new string[result.Count()][]; // создаём сисок
                    int i = 0;
                    foreach (var item in result) // заполняем список
                    {
                        list[i] = new string[5];
                        list[i][0] = item.Id;
                        list[i][1] = item.Text;
                        var classname = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey);
                        list[i][2] = classname.Count()>0?classname.First().Name:"Личная запись";
                        list[i][3] = ac.GetFirstNameById(item.UserId) + " " + ac.GetLastNameById(item.UserId);
                        list[i][4] = item.CreatedAt.GetValueOrDefault().AddHours(3).ToString("dd MMM yyyy HH:mm", new CultureInfo("ru-UA"));
                        i++;
                    }
                    return list;
                }
                else
                {
                    int i = 0;
                    var result = cont.Records.Where(rec => rec.UserId == usid).OrderByDescending(rec => rec.CreatedAt).ToArray();
                    var list = new string[result.Count()][];
                    foreach (var item in result)
                    {
                        list[i] = new string[5];
                        list[i][0] = item.Id;
                        list[i][1] = item.Text;
                        var classname = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey);
                        list[i][2] = classname.Count() > 0 ? classname.First().Name : "Личная запись";
                        list[i][3] = ac.GetFirstNameById(item.UserId) + " " + ac.GetLastNameById(item.UserId);
                        list[i][4] = item.CreatedAt.GetValueOrDefault().AddHours(3).ToString("dd MMM yyyy HH:mm", new CultureInfo("ru-UA"));
                        i++;
                    }
                    return list;
                }
            }
            return new string[0][];
        }

        private bool AddSubj(string Token, string Text, string ClassKey)
        {
            if (ac.CheckToken(Token))
            {
                var data = new DataObjects.Record()
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = Text,
                    UserId = ac.GetId(Token),
                    ClassKey = ClassKey,
                    UserType = ac.GetType(Token)
                };
                cont.Records.Add(data);
                cont.SaveChanges();
                // запись обновления
                var updated = new DataObjects.UpdatedList()
                {
                    Id = Guid.NewGuid().ToString(),
                    RecordsClass = ClassKey
                };
                cont.UpdatedLists.Add(updated);
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private bool DelSubj(string Token, string RecId)
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                var result = cont.Records.Where(rec => rec.UserId == usid)
                    .Where(rec => rec.Id == RecId);
                if (result.Count() > 0)
                {
                    var data = result.First();
                    cont.Records.Remove(data);
                    cont.SaveChanges();
                    return true;
                }
            }
            return false;
        }
    }
}
