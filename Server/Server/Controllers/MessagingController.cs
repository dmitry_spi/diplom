﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class MessagingController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();

        public bool Post(string Action, string Token, string Text, string ClassKey)
        {
            switch (Action)
            {
                case "newmes":
                    return NewMessage(Token, Text, ClassKey);
            }
            return false;
        }

        public string[][] Post(string Token, string ClassKey, string Text) 
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                var check_user = cont.ClassMembers.Where(classmem => classmem.ClassId == ClassKey)
                    .Where(classmem => classmem.MemberId == usid).Count();
                var check_tracher = cont.MyClasses.Where(myclasses => myclasses.Key == ClassKey)
                    .Where(myclasses => myclasses.Author == usid).Count();
                if (check_user > 0 || check_tracher > 0)
                {
                    var result = cont.Messages.Where(mes => mes.ClassKey == ClassKey).OrderBy(mes => mes.CreatedAt).Take(50);
                    if (result.Count() > 0)
                    {
                        var list = new string[result.Count()][];
                        int i = 0;
                        string lasttext="none";
                        foreach (var mes in result.ToArray())
                        {
                            list[i] = new string[3];
                            list[i][0] = mes.Text;
                            lasttext = mes.Text;
                            list[i][1] = ac.GetFirstNameById(mes.UserId);
                            list[i][2] = ac.GetLastNameById(mes.UserId);
                            i++;
                        }
                        if (lasttext.CompareTo(Text) != 0)
                        {
                            return list;
                        }
                    }
                }
            }
            return new string[0][];
        }

        private bool NewMessage(string Token, string Text, string ClassKey) //новое сообщение
        {
            if (ac.CheckToken(Token))
            {
                var usid = ac.GetId(Token);
                var check_class_key = cont.MyClasses.Where(myclasses => myclasses.Key == ClassKey).Count();
                var check_user = cont.ClassMembers.Where(classmem => classmem.ClassId == ClassKey)
                    .Where(classmem => classmem.MemberId == usid).Count();
                var check_tracher = cont.MyClasses.Where(myclasses => myclasses.Key == ClassKey)
                    .Where(myclasses => myclasses.Author == usid).Count();
                if (check_class_key > 0 && (check_user > 0 || check_tracher > 0))
                {
                    var message = new DataObjects.Message()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Text = Text,
                        ClassKey = ClassKey,
                        UserId = usid
                    };
                    cont.Messages.Add(message);
                    cont.SaveChanges();
                    // запись обновления
                    var updated = new DataObjects.UpdatedList()
                    {
                        Id = Guid.NewGuid().ToString(),
                        MessagesClass = ClassKey
                    };
                    cont.UpdatedLists.Add(updated);
                    cont.SaveChanges();
                    return true;
                }
            }
            return false;
        }
    }
}
