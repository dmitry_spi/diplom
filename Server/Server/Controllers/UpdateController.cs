﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class UpdateController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();
        WorkWithClassController wcc = new WorkWithClassController();

        public List<string> Post(string Token, string Date)
        {
            List<string> list = new List<string>();
            if (ac.CheckToken(Token))
            {
                DateTimeOffset date = new DateTimeOffset();
                date = DateTimeOffset.Parse(Date);
                var ClassKeys = ac.GetType(Token) == 0 ? wcc.GetClassKeyList(ac.GetId(Token)) : wcc.GetClassKeyListForAuth(ac.GetId(Token));
                int i = 0;
                foreach (var key in ClassKeys)
                {
                    var result = cont.UpdatedLists.Where(ul => ul.AdsClass == key).Where(ul => ul.CreatedAt > date);
                    if (result.Count() > 0)
                    {
                        list.Add("Ads");
                    }
                    result = cont.UpdatedLists.Where(ul => ul.EventsClass == key).Where(ul => ul.CreatedAt > date);
                    if (result.Count() > 0)
                    {
                        list.Add("Events");
                    }
                    result = cont.UpdatedLists.Where(ul => ul.MessagesClass == key).Where(ul => ul.CreatedAt > date);
                    if (result.Count() > 0)
                    {
                        list.Add("Messages");
                    }
                    result = cont.UpdatedLists.Where(ul => ul.RecordsClass == key).Where(ul => ul.CreatedAt > date);
                    if (result.Count() > 0)
                    {
                        list.Add("Records");
                    }
                    result = cont.UpdatedLists.Where(ul => ul.SchedulesClass == key).Where(ul => ul.CreatedAt > date);
                    if (result.Count() > 0)
                    {
                        list.Add("Schedules");
                    }
                    i++;
                }
            }
            return list;
        }
    }
}
