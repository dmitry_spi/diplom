﻿using Microsoft.Azure.Mobile.Server.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.Controllers
{
    [MobileAppController]
    public class AdsController : ApiController
    {
        Models.MobileServiceContext cont = new Models.MobileServiceContext();
        AuthController ac = new AuthController();
        WorkWithClassController wcc = new WorkWithClassController();

        public bool Post(string Action, string Token, string Text, string ClassKey, string AdId)
        {
            switch(Action)
            {
                case "add":
                    return AddAd(Token, Text, ClassKey);
                case "del":
                    return DelAd(Token, AdId);
            }
            return false;
        }

        public string[][] Post(string Token)
        {
            if (ac.CheckToken(Token)) // проверяем токен
            {
                var ClassKeys = ac.GetType(Token)==0? wcc.GetClassKeyList(ac.GetId(Token)): wcc.GetClassKeyListForAuth(ac.GetId(Token));
                string first_class_key = ClassKeys.Count()>0?ClassKeys[0]:"0";
                var temp_result = cont.Ads.Where(ad => ad.ClassKey == first_class_key);
                if (ClassKeys.Count() > 1)
                {
                    foreach (var key in ClassKeys.Skip(1))
                    {
                        temp_result = temp_result.Concat(cont.Ads.Where(ad => ad.ClassKey == key));
                    }
                }
                if (temp_result.Count() > 0)
                {
                    var result = temp_result.OrderByDescending(ad=>ad.CreatedAt).ToArray(); // сортируем и делаем массив
                    //формируем список
                    var list = new string[result.Count()][];
                    int i = 0;
                    foreach (var item in result)
                    {
                        list[i] = new string[5];
                        list[i][0] = item.Id;
                        list[i][1] = item.Text;
                        list[i][2] = cont.MyClasses.Where(myclass => myclass.Key == item.ClassKey).First().Name; // название класса
                        list[i][3] = ac.GetLastNameById(wcc.GetAuthIdByKey(item.ClassKey)); // фамилия автора
                        list[i][4] = item.CreatedAt.GetValueOrDefault().ToString("dd MMM yyy", new CultureInfo("ru-UA"));
                        i++;
                    }
                    return list;
                }
            }
            return new string[0][];
        }

        private bool AddAd(string Token, string Text, string ClassKey) // добавляем запись
        {
            if (ac.CheckToken(Token) && wcc.CheckAuth(ClassKey, ac.GetId(Token))) // проверяем токен и владельца класса
            {
                var data = new DataObjects.Ad //создаём запись
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = Text,
                    ClassKey = ClassKey
                };
                cont.Ads.Add(data); // добавляем
                cont.SaveChanges(); // сохраняем
                // запись обновления
                var updated = new DataObjects.UpdatedList()
                {
                    Id = Guid.NewGuid().ToString(),
                    AdsClass = ClassKey
                };
                cont.UpdatedLists.Add(updated);
                cont.SaveChanges();
                return true;
            }
            return false;
        }

        private bool DelAd(string Token, string AdId) // удаляем запись
        {
            if (ac.CheckToken(Token))
            {
                var data = cont.Ads.Where(ad => ad.Id == AdId).First(); // находим запись по ID
                if (wcc.CheckAuth(data.ClassKey, ac.GetId(Token))) // проверяем автора
                {
                    cont.Ads.Remove(data); // извлекаем запись
                    cont.SaveChanges(); // сохраняем
                    return true;
                }
            }
            return false;
        }
    }
}
