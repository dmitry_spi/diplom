﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class MyClass : EntityData
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Key { get; set; }
    }
}