﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class UpdatedList : EntityData
    {
        public string AdsClass { get; set; }
        public string EventsClass { get; set; }
        public string MessagesClass { get; set; }
        public string RecordsClass { get; set; }
        public string SchedulesClass { get; set; }
    }
}
