﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class Message : EntityData
    {
        public string Text { get; set; }
        public string ClassKey { get; set; }
        public string UserId { get; set; }
    }
}