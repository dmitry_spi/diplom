﻿using Microsoft.Azure.Mobile.Server;
using System;

namespace Server.DataObjects
{
    public class Event : EntityData
    {
        public string Name { get; set; }
        public string When { get; set; }
        public string Where { get; set; }
        public string ClassKey { get; set; }
    }
}