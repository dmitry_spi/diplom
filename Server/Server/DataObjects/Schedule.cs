﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class Schedule : EntityData
    {
        public string DayOfWeek { get; set; }
        public string Number { get; set; }
        public string Subject { get; set; }
        public string ClassKey { get; set; }
        public string UserId {get; set;}
        public int UserType { get; set; }
    }
}