﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class User : EntityData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Token { get; set; }

        //public User()
        //{
        //    Username = "default";
        //    Password = "default";
        //    Type = 0;
        //    FirstName = "default";
        //    LastName = "default";
        //    Token = "default";
        //}
    }
}