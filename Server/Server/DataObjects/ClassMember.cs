﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class ClassMember: EntityData
    {
        public string ClassId { get; set; }
        public string MemberId { get; set; }
    }
}