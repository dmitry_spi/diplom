﻿using Microsoft.Azure.Mobile.Server;

namespace Server.DataObjects
{
    public class Ad : EntityData
    {
        public string Text { get; set; }
        public string ClassKey { get; set; }
    }
}