﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.DataObjects
{
    public class Leaner
    {
        string UserId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Patronimic { get; set; }
    }
}