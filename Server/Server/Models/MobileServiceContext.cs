using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server.Tables;

namespace Server.Models
{
    public class MobileServiceContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
        //
        // To enable Entity Framework migrations in the cloud, please ensure that the 
        // service name, set by the 'MS_MobileServiceName' AppSettings in the local 
        // Web.config, is the same as the service name when hosted in Azure.

        private const string connectionStringName = "Name=MS_TableConnectionString";

        public MobileServiceContext() : base(connectionStringName)
        {
        }

        public DbSet<DataObjects.TodoItem> TodoItems { get; set; }
        public DbSet<DataObjects.User> Users { get; set; }
        public DbSet<DataObjects.MyClass> MyClasses { get; set; }
        public DbSet<DataObjects.ClassMember> ClassMembers { get; set; }
        public DbSet<DataObjects.Message> Messages { get; set; }
        public DbSet<DataObjects.Schedule> Schedules { get; set; }
        public DbSet<DataObjects.Ad> Ads { get; set; }
        public DbSet<DataObjects.Event> Events { get; set; }
        public DbSet<DataObjects.Record> Records { get; set; }
        public DbSet<DataObjects.UpdatedList> UpdatedLists { get; set; }
        public DbSet<DataObjects.Parent> Parents { get; set; }
        public DbSet<DataObjects.TypeUser> TypeUsers { get; set; }
        public DbSet<DataObjects.Leaner> Leaners { get; set; }
        public DbSet<DataObjects.Teacher> Teachers { get; set; }
        public DbSet<DataObjects.Subject> Subjects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
        }
    }
}
